<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// [ 应用入口文件 ]
session_start();
if(version_compare(PHP_VERSION,'5.5','<'))  die('require PHP > 5.6 !');
error_reporting(E_ERROR | E_WARNING | E_PARSE);//报告运行时错误
if(file_exists("./install/") && !file_exists("./install/install.lock")){
    header('Location:/install/index.php');
    exit();
}
define('BIND_MODULE','member');
// 定义应用目录
define('APP_PATH', __DIR__ . '/../app/');
// 加载框架引导文件
//require('./ismobile.php');
require __DIR__ . '/../thinkphp/start.php';
